from django.db import models
from django.forms import ModelForm


class User(models.Model):
    username = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField(max_length=70, blank=True, null=True, unique=False)
    password = models.CharField(max_length=30)

    def __str__(self):
        return self.username

    def __unicode__(self):
        return self.username


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'last_name', 'email', 'password']


