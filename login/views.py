from Tools.scripts.make_ctype import method
from django.contrib.auth import authenticate
from django.shortcuts import render, redirect
from login.models import UserForm
from django.contrib import messages
from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from .models import User
from django.template import Context


def reg(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        form.save()
        messages.success(request, f' Your account is successfully created!')
        return redirect('login')
    else:
        form = UserForm
    return render(request, 'login/register.html', {'form': form})


@login_required
def profile(request):
    current_user = request.user
    username = {"name": User.objects.get(username=current_user.username)}
    return render(request, 'login/profile.html', context=username)


def logout(request):
    return render(request, 'login/logout.html')

