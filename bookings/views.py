from django.contrib import messages
from django.shortcuts import render, redirect
from .models import BookingForm, booking_details



# Create your views here.
def book_ticket(request):
    if request.method == 'POST':
        form = BookingForm(request.POST)
        if form.is_valid():
            data = [form.cleaned_data['zero'], form.cleaned_data['one'], form.cleaned_data['two']]
            form1 = form.save(commit=False)
            form1.total = sum(data)

            form1.save()
            messages.success(request, f' you booked ticket successfully!')
            return redirect('profile')
    else:
        form = BookingForm
    return render(request, 'bookings/booking_first_page.html', {'form': form})


def booked_ticket_view(request):
    current_user = request.user
    username = {"name": booking_details.objects.get(id=11)}
    return render(request, 'bookings/booked ticket page.html', context=username)

