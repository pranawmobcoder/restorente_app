from django.db import models
from django.forms import ModelForm


class booking_details(models.Model):
    username = models.CharField(max_length=20)
    zero = models.IntegerField(max_length=3, null=False, default=0)
    one = models.IntegerField(max_length=3, null=False, default=0)
    two = models.IntegerField(max_length=3, null=False, default=0)
    three = models.IntegerField(max_length=3, null=False, default=0)
    four = models.IntegerField(max_length=3, null=False, default=0)
    five = models.IntegerField(max_length=3, null=False, default=0)
    six = models.IntegerField(max_length=3, null=False, default=0)
    seven = models.IntegerField(max_length=3, null=False, default=0)
    eight = models.IntegerField(max_length=3, null=False, default=0)
    nine = models.IntegerField(max_length=3, null=False, default=0)
    total = models.IntegerField(max_length=4, null=False, default=0)

    def __str__(self):
        return self.username

    def __unicode__(self):
        return self.username


class BookingForm(ModelForm):
    class Meta:
        model = booking_details
        fields = ['username', 'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'total']
