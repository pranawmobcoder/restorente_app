from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path("book-ticket", views.book_ticket, name='book-ticket'),
    path("booked ticket activity", views.booked_ticket_view, name='booked-ticket-activity')


]
